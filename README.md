**Devices Benchmarked:**
- [Desktops](#dekstops)
	- [Amd 3600X](#amd3600)
- [MiniPC](#minipc)
 	- [Minisforum Venus UM790 Pro](#UM790PRO)
 	- [Minisforum HX99G](#HX99G)
	- [Minisforum Venus UM773](#UM773)
	- [Beelink mini s12](#s12)
	- [TRIGKEY S5](#s5)
	- [Beelink SEi12](#sei12)
	- [Minisforum UM690](#UM690)
	- [Beelink SER5](#ser5)
	- [Beelink U59 PRO](#u59pro)
	- [Beelink GTR5](#gtr5)
	- [Minisforum UM590](#um590)
	- [Zotac ER51070](#zotac)
	- [Minisforum Elite](#minielite)
- [Laptops](#laptops)
	- [Dell 7320 Detachable](#dell7320)
	- [Thinkpad X13s Amd](#x13s)
	- [Lenovo T480](#t480)
	- [Huawei Matebook 14](#mate14)
	- [Apple Macbook M1 Pro 14"](#m1pro)
	- [Apple Macbook Intel 16"](#mpro16intel)
	- [Lenovo T480s](#t480s)
	- [Framework Intel 12th gen](#frame12)
- [Servers](#server)
	- [Gigabyte MB10](#mb10)
	- [Beelink GTR5 Proxmox](#gtr5proxmox)
	- [Dell T330 Proxmox](#t330)
	- [Dell T320 Proxmox](#t320)
	- [Dell R510 Proxmox](#r510)
	- [HP Microserver Gen8](#gen8)
	- [Beelink SR5 Virtualbox](#sr5)
	- [Supermicro X9SPV-M4-3UE](#superm)
- [Cloud](#cloud)
	- [IBM Bare Metal](#ibm)
	- [Aws](#aws)
	- [Equinix](#equinix)
	- [Webdock](#webdock)
	- [SSD Nodes](#ssdnodes)
	- [Hetzner Cloud](#hetzner)
	- [Civo](#civo)
- [Boards](#boards)
	- [Pi4](#pi4)


<!-- headings -->
<a id="command"></a>
command `phoronix-test-suite benchmark build-linux-kernel-1.14.0`

<a id="deskops"></a>
# Desktops

<a id="amd3600x"></a>
## Amd 3600x
**UAE Build times**
Desktop Nick P
Ryzen 3600X 6c/12th /64gb3200mhz 5.0.3 **1h34m**
(Νίκος Παττακός, [10/08/2022 12:14]
Περίπου 94m06s
***

<a id="minipc"></a>
# Mini PC

<a id="UM790PRO"></a>
## **Minisforum UM 790 PRO**
Amd 7940HS 8c/16th , 64gb ram ddr5
Perf bios
4.7ghz, 83c audible maybe ok 
w11 passmark
cpu 32,477 
https://www.passmark.com/baselines/V11/display.php?id=209742020225

wsl
5.18 defconfig **72.19**
5.18 allmodconfig **901.88**
https://openbenchmarking.org/result/2405090-NE-UM790PRO080

u24.04
5.18 defconfig **91.72**
5.18 allmodconfig **1075.24**
https://openbenchmarking.org/result/2405091-NE-UM676894559

proxmox
4c , 16gb vm debian testing 
5.18 defconfig **91.72**
5.18 allmodconfig **1075.24**

***
<a id="HX99G"></a>
## **Minisforum HX99G**
Amd 6900hx 8c/16th , 32gb ram ddr5, 6650u dgpu
54w bios
4.2-4.3ghz, 78c audible but very much ok 
w11 passmark
cpu 26712/25,051 
https://www.passmark.com/baselines/V11/display.php?id=200914904224
https://www.passmark.com/baselines/V11/display.php?id=200935928674

wsl
5.18 defconfig **92.49**
https://openbenchmarking.org/result/2401282-NE-TESTPERF642
5.18 allmodconfig **1142.34**
https://openbenchmarking.org/result/2401296-NE-ALL54W96781

u23.10
5.18 defconfig **105.57**
5.18 allmodconfig **1251.37**
https://openbenchmarking.org/result/2401290-NE-PERF54WAL83
***
<a id="UM773"></a>
## **Minisforum venus UM773**
(Amd 7735hs 8c/16th . 32gb ddr5)
54w bios
4.1ghz, 83c
U23.04server
5.18 defconfig **119.91**
5.18 allmodconfig **1446.44**
https://openbenchmarking.org/result/2310087-NE-7735HS83039

w11 wsl 
5.18 defconfig **94.47**
5.18 allmodconfig **1190.48**
https://openbenchmarking.org/result/2310082-NE-WSLUM773794

u23.10beta gnome perf mode
5.18 defconfig **107.80**
5.18 allmodconfig **1299.41**
https://openbenchmarking.org/result/2310111-NE-PERFUM77352

u23.10beta gnome balanced mode
5.18 defconfig **109.20**
5.18 allmodconfig **1299.35**
https://openbenchmarking.org/result/2310116-NE-BALANCEDU22

Passmark v11  
cpu mark 25937/26,055 
passmark 6018/5931

https://www.passmark.com/baselines/V11/display.php?id=192044326757

https://www.passmark.com/baselines/V11/display.php?id=192044900520
***

<a id="s12"></a>
## **Beelink mini s12**
(Intel N100 4c/4th, 16gb , nvme)
Linux Debian 12 (proxmox) , Bios perf, 2.9 ghz 70c
5.18 defconfig **427.42**
5.18 allmodconfig **5311.73**
https://openbenchmarking.org/result/2310044-NE-BEELINKN138
***

<a id="s5"></a>
## **TRIGKEY S5**
(5560u 6c/12th, 16gb , nvme)
W11 Pro , Bios 25w , smartshift enabled ,3.9ghz spikes drops to 3.5ghz
Passmark
https://www.passmark.com/baselines/V10/display.php?id=171348018128
wsl2 ubuntu 20,04
5.18 defconfig **212.27** (153-165 for the first 6 runs then jumped to 300)
5.18 allmodconfig **2270.53**
https://openbenchmarking.org/result/2301064-NE-TESTALL2586

Linux U22.04.1, Bios 25w, 3.2 ghz 81c
5.18 defconfig **154.68**
5.18 allmodconfig **2015.94**
https://openbenchmarking.org/result/2301069-NE-UBUNTUALL02
Full load noice terrible for desktop in windows , sillent for linux , plex perf not great
***

<a id="sei12"></a>
## **Beelink SEi12**
(i5-1235U 10c/12th, 32gb , nvme)( 8 efficient cores , 2 Perf Cores)

W11 Pro , Default bios 12c X 1.31ghz
Passmark
https://www.passmark.com/baselines/V10/display.php?id=171301542261
wsl2 ubuntu 20,04
5.18 defconfig **171.51**
5.18 allmodconfig **2475.39**
https://openbenchmarking.org/result/2301050-NE-TESTALLDE80

Linux U22.04.1, Default bios , 2.6 ghz 78c
5.18 defconfig **144.23**
5.18 allmodconfig **1856.17**
https://openbenchmarking.org/result/2301060-NE-TESTALLDE27

loud fans , plex perf excellent 2x4k transcodes with no sweat
***

<a id="sei12"></a>
## **Minisforum UM690**
(6900HX 8c/16th, 32gb , nvme)

W11 Pro , 54W bios 4.00ghz
Passmark
https://www.passmark.com/baselines/V10/display.php?
id=171276322131

wsl2 ubuntu 20,04
5.18 defconfig **102.89**
5.18 allmodconfig **1314.73**
https://openbenchmarking.org/result/2301051-NE-TESTALL5445

Linux U22.04.1, 54W bios 4.00ghz 80c
5.18 defconfig **93.31**
5.18 allmodconfig **1164**
https://openbenchmarking.org/result/2301054-NE-TESTALL5425
***

<a id="ser5"></a>
## **Beelink SER5**
(5560u 6c/12th , 16gb , nvme)
Bios at 25W performance mode almost silent in real life

stock settings max clock all cores 2.72ghz
5.18 defconfig **177.40**
5.18 allmodconfig **2246.36**
https://openbenchmarking.org/result/2212166-NE-BEELINKSR02

Bios tweaks to the fan curve still not noicy , max clock all cores 2.94ghz
5.18 defconfig **177.35**
5.18 allmodconfig **2238.09**
https://openbenchmarking.org/result/2212165-NE-BEELINKSR23

random test on arch
https://openbenchmarking.org/result/2306281-NE-SR5ARCH5838

VM on boxes while using the system
(silent in office, 80c , 3.4ghz all cores)
5.18 defconfig **263.00**
5.18 allmodconfig **2634.72**
https://openbenchmarking.org/result/2310061-NE-VMBOXES5594
***

<a id="u59pro"></a>
## **Beelink U59 PRO**
10 W
(intel N5105 4c/4th , 16gb , nvme)
W11 Pro
Passmark
https://www.passmark.com/baselines/V10/display.php?id=171848338816
wsl2
5.18 defconfig **1**
5.18 allmodconfig **1**

stock settings max clock all cores 2.8ghz 77c
5.18 defconfig **514.10**
5.18 allmodconfig **6483.16**
https://openbenchmarking.org/result/2212181-NE-BEELINKU501

New Test
stock settings max clock all cores 2.8ghz 77c
5.18 defconfig **496.93**
5.18 allmodconfig **6383.71**

Perfect transocding box
***

<a id="gtr5"></a>
## **Beelink GTR5**
(5900hx@ 3.3ghz 8c/16th , 32gb , nvme)

Bios on AUTO Power Mode (3.2-3.3ghz)
5.18 defconfig **117.85**
5.18 allmodconfig **1479.13**
https://openbenchmarking.org/result/2208176-NE-RYZEN590024
https://openbenchmarking.org/result/2208174-NE-RYZEN590093

Bios on 45W Perf Mode (3.4-3.5ghz) 81c max temp , fans audible but ok -- better than minisforum MINIS FORUM UM590

5.18 defconfig **106.17**
5.18 allmodconfig **1368.20**
5.18 allmodconfig **1373.09** (silent fan curve)

https://openbenchmarking.org/result/2208229-NE-5900BEELI66
https://openbenchmarking.org/result/2208226-NE-5900HZBEE95
https://openbenchmarking.org/result/2208222-NE-5900HXBEE39

W11 Pro 45W
Clocks ~3.6ghz
Passmark
https://www.passmark.com/baselines/V10/display.php?id=171103359601
WSL 2
5.18 defconfig **117.10**
5.18 allmodconfig **1583.39**
https://openbenchmarking.org/result/2301033-NE-ALLTESTS485

Linux rerun Ubuntu server 22.04.1
@3.4 83c
5.18 defconfig **108.93**
5.18 allmodconfig **1361.79**
https://openbenchmarking.org/result/2301031-NE-5900HXALL25

**UAE Build times**
(5900hx@ 3.3gbhz , 32gb , nvme)  5.0.3 **1h30m32s**
***

<a id="um590"></a>
## **Minisforum UM590**
(5900hx@ 3.3ghz 8c/16th , 32gb , nvme)
Loud fans
***
<a id="zotac"></a>
## **Zotac ER51070**
srv-gr-005 (Ryzen 1400 4c/8th 3.2ghz/32gb ram nvme)
5.18 defconfig **283,00**
5.18 allmodconfig **3631.46**
https://openbenchmarking.org/result/2208168-NE-480KERNEL65
https://openbenchmarking.org/result/2208167-NE-ZOTACKERN03

**UAE Build times**
srv-gr-005 (Ryzen 1400 4c/8th 3.2ghz/32gb ram nvme)
5.0.2  **4h7m**
5.0.3 **4h6m**
old test 4h32m
***
<a id="minielite"></a>
## **MinisForum Elite**
(5900hx 8c/16th 4ghz/32gb/512nvme)
**UAE Build times**
5.0.3 **1h20m50s**
1h21m40s
***
<a id="laptops"></a>
# **Laptops**
 
<a id="dell7320"></a>
## **Dell Latitude 7320 Detachable**
(i7-1180G7 16gb ram , 256nvme)
Endeavour Linux 
Quiet Mode bios under battery  ( 2ghz, 73c ,fan noise acceptable ) 
5.18 defconfig **426.46**
5.18 defconfig **426.21**
https://openbenchmarking.org/result/2401191-NE-BATTERYQU87
https://openbenchmarking.org/result/2401198-NE-QUIETPERF59

UltraPerf Mode bios under battery  ( 2.1 ghz, 73c ,fan noise acceptable )
( cpupower-gui set to built in perf mode)
https://openbenchmarking.org/result/2401199-NE-TESTPERFB77
5.18 defconfig **415.53**

UltraPerf Mode bios plugged in   ( 2.7 ghz, 99c boost then down to 2.1  ,fan noise loud )
( cpupower-gui set to built in perf mode)
5.18 defconfig **408.5**
https://openbenchmarking.org/result/2401191-NE-PERFPOWER24

w11
Passmark W11 home 
Optimized mode bios plugged in (very loud under load)
Cpu  9353 
https://www.passmark.com/baselines/V11/display.php?id=200080952652
Quiet Mode bios under battery (non audible under load)
Cpu 8391
https://www.passmark.com/baselines/V11/display.php?id=200083228665

UltraPerf Mode bios plugged in 
5.18 defconfig **360.10**
https://openbenchmarking.org/result/2401198-NE-PLUGGEDPE76
***

<a id="x13s"></a>
## **Thinkpad x13s AMD Gen3**
(6650u 16gbram/512nvme)

Passmark 11   W11
17306 cpu 
https://www.passmark.com/baselines/V11/display.php?id=192029241167

u23.04 Gnome
5.18 defconfig **192.22**
5.18 allmodconfig **2389.02**
https://openbenchmarking.org/result/2310084-NE-THINKPADX49

wsl w11
5.18 defconfig **183.94**
5.18 allmodconfig **2549.83**
https://openbenchmarking.org/result/2310086-NE-WSLTHINKP20
***

<a id="mate14"></a>
## **Matebook 14 Huawei**
Amd 5700u 8c/16th , 16gb DDR4 3200, nvme)

stock settings max clock all cores 3.6ghz 89c
5.18 defconfig **169.65**
5.18 allmodconfig **2049.96**
https://openbenchmarking.org/result/2212278-NE-5700UMATE87
***
<a id="m1pro"></a>
## **Macbook M1 Pro 8C 14gpu 14"**
UTM VM Ubuntu 8c/16ram
5.18 defconfig **320.52**
https://openbenchmarking.org/result/2209309-NE-TEST0339335
5.18 allmodconfig **1632.47**
https://openbenchmarking.org/result/2209305-NE-TEST0447585

Lima VM Ubuntu 8c/16ram
5.18 defconfig **315.46**
https://openbenchmarking.org/result/2209301-NE-TEST0263854
5.18 allmodconfig **1591.40**
https://openbenchmarking.org/result/2209302-NE-TEST0110744

Parallels Ubuntu VM
5.18 defconfig **288.15**
5.18 allmodconfig **1464.81**
https://openbenchmarking.org/result/2311094-NE-M1UBUNTUV36
5.18 defconfig **306.09**
5.18 allmodconfig **1554.12**
https://openbenchmarking.org/result/2311094-NE-X86VMPARA18
***

<a id="t480"></a>
## **Lenovo t480**
T480 (8250u 4c/8th 2.5ghz/64gb ram nvme)
5.18 defconfig **421.90** / **408.367**
5.18 allmodconfig **5412.19**
https://openbenchmarking.org/result/2208161-NE-T480SHORT65
https://openbenchmarking.org/result/2208162-NE-T480LONGS12
WSL2 in same laptop
5.18 defconfig **288.88**
5.18 allmodconfig **4006.86**
https://openbenchmarking.org/result/2208248-NE-WSL2T480S68
https://openbenchmarking.org/result/2208247-NE-WSL2T480F45
https://openbenchmarking.org/result/2208248-NE-WSL2T480L96
***

<a id="mpro16intel"></a>
## **Macbook Pro 16.1 Intel**
Model Name:MacBookPro 16,1
Processor Name: 6-Core Intel Core i7 9750H,2.6 GHz,16gb ram
5.18 defconfig **222.21** ( inside rancher container)
https://openbenchmarking.org/result/2208166-NE-KERNELMAC39
136634
***
<a id="t480s"></a>
## **Lenovo T480s**
T480s (8250u 4c/8th 2.5ghz/48gb ram nvme) 
**UAE Build times**
5.0.2  **4h16m**
***

<a id="frame12"></a>
## **Framework Intel 12th gen**
Framework Laptop ( External)
(I5 1240p 12c/16th 32gb ram / 512nvme) 5.0.3 **1h46m38s**
***

<a id="servers"></a>
# Servers

<a id="gtr5proxmox"></a>
# **Beelink GTR5 Proxmox**
**vm**
Proxmox 8c/32gb vm on Beelink GTR5
5.18 defconfig **158.27**
https://openbenchmarking.org/result/2301208-NE-TESTVM8CO19
5.18 defconfig Host CPU **132**
https://openbenchmarking.org/result/2306287-NE-GTR58CHOS53

**vm**
Proxmox 4c/8gb vm on Beelink GTR5
3.3ghz 5900hx
5.18 defconfig **243.23**
https://openbenchmarking.org/result/2302285-NE-VMDEVVM0096
5.18 defconfig **232.76**
https://openbenchmarking.org/result/2306284-NE-GTR5PROXM78

**UAE Build times**
proxmox vm  GTR5 
(4c,8gb.150gb disk)
5.0.3 **3h09m**

proxmox vm  GTR5 
(8c,16gb.150gb disk)
5.0.3 **1h55m**
***

<a id="mb10"></a>
## **Gigabyte MB10**
(Xeon D-1521 4c/8th . 32gb ddr4 ecc)
under proxmox 8.1.3
stock bios
2.7ghz,92c

5.18 defconfig **401.33**
5.18 allmodconfig **4920.08**
https://openbenchmarking.org/result/2311248-NE-PROXMOXTE39
***

<a id="t330"></a>
## **Dell T330 Proxmox**
**vm**
Proxmox 4c/32gb vm on Dell T330
2.9ghz 1260L v5
5.18 defconfig **331.18**
5.18 allmodconfig **4406.04**
https://openbenchmarking.org/result/2306217-NE-VMDELLT3364

**vm**
Proxmox 8c/32gb vm on Dell T330
2.9ghz 1260L v5
5.18 defconfig **302**
5.18 allmodconfig **4026.1**
https://openbenchmarking.org/result/2306215-NE-VMDELLT3325

**vm**
Proxmox 4c/16gb vm w11 on Dell t330 
https://www.passmark.com/baselines/V11/display.php?id=184793925886
***
<a id="sr5"></a>
## **Beelink SR5 Virtualbox**
**vm**
Virtualbox 8c/16gb vm on Beelink SR5
2.2ghz 5560u
5.18 defconfig **204.98**
5.18 allmodconfig **2606.6**
https://openbenchmarking.org/result/2306221-NE-VM8C16GBS11
***
<a id="superm"></a>
## **Supermicro X9SPV-M4-3UE**
 i7-3517UE 2c/4th 1.70GHz 16gb ddr3

**Proxmox vm**
Proxmox 4c/8gb vm
5.18 defconfig **895.83**
https://openbenchmarking.org/result/2307145-NE-VMSUPERMI66
***

<a id="cloud"></a>
# Cloud

<a id="ibm"></a>
# **IBM Bare Metal Instance**
2 x 8260 CPU @ 2.40GHz 48c / 96th 2x16gb DDR4
700USD pm
 
Debian 10
5.18 defconfig **45.02**
5.18 allmodconfig **482.18**
https://openbenchmarking.org/result/2212223-NE-2X8260FUL62
Code Compile Benchmark (partly failed)
https://openbenchmarking.org/result/2212234-NE-DEVBENCHM29

Ubuntu 20.04
5.18 defconfig **49.33**
5.18 allmodconfig **483.65**
[https://openbenchmarking.org/result/2306028-NE-TESTJUNE227](https://openbenchmarking.org/result/2306028-NE-TESTJUNE227 "June 2023 test")
***

<a id="aws"></a>
# **AWS**
t2xlarge 4c /16gb ram /30gb 
5.18 defconfig **417.87**
5.18 allmodconfig **5554.75**

https://openbenchmarking.org/result/2307216-NE-T2XLTESTA44
***

<a id="equinix"></a>
# **Equinix**
**UAE Build times**

Equinix dual build
srv-met-002 ( 2 x Intel Xeon Gold 6338 128c 2.6ghz/1tb ram , ssd )  5.0.2 **33m26s**

Equinix single build
srv-met-002 ( 2 x Intel Xeon Gold 6338 128c 2.6ghz/1tb ram , ssd )  5.0.2 **22m10s**

Equinix
srv-met-001 ( epyc7402p 24c 3.3ghz/64gb ram , ssd ) 5.0.2 **40m55s**
***
<a id="webdock"></a>
# **Webdock**
**UAE Build times**
Webdock single build 
SSD Pro Plus (intelXeon 12.5c/25th 30gb ram, 300ssd) 5.0.3 **1h20m22s**

Webdock dual build
SSD Pro Plus (intelXeon 12.5c/25th 30gb ram, 300ssd) 5.0.3 **2h6m30s**
***

<a id="ssdnodes"></a>
# **SSDNodes**
**UAE Build times**
Ssdnodes 
srv-nl-001 (12c 2.1ghz/96gb ram nvme ) 5.0.2 **3h35m**
***
<a id="hetzner"></a>
# **Hetzner Cloud**
ARMv8 Neoverse-N1 (4 Cores) ,8gb ram 
5.18 defconfig **4607.68**
https://openbenchmarking.org/result/2401251-NE-TESTARM7039

ARMv8 Neoverse-N1 (16 Cores) ,32gb ram CAX41 shared
5.18 defconfig **1027.79**
https://openbenchmarking.org/result/2401253-NE-TEST2024061

Amd (8 Cores) ,16gb ram CPX41 shared
5.18 defconfig **183.39**
https://openbenchmarking.org/result/2401251-NE-DEFAULT9891

Intel (8 Cores) ,32gb ram CX51 shared
5.18 defconfig **254.69**
https://openbenchmarking.org/result/2401254-NE-FGERGEREG01

Amd (8 Cores) ,32gb ram CCX33 dedicated
5.18 defconfig **350.81**
https://openbenchmarking.org/result/2401251-NE-DEFAULT3352
***

<a id="civo"></a>
# **Civo**
1core ,1gb ram 
5.18 defconfig **1372.58**
Allmodconfig dnf
https://openbenchmarking.org/result/2403296-NE-BENCH003399

6core ,16gb ram 
5.18 defconfig **237.02**
5.18 allmodconfig **3006.15**
https://openbenchmarking.org/result/2403297-NE-BENCH001309

***
<a id="boards"></a>
# Boards

<a id="pi4"></a>
## **Pi4**
8gb @1.8ghz, usb ssd, passively cooled 72c
5.18 defconfig **5240.06**
https://openbenchmarking.org/result/2208235-NE-TESTRASPI94
***